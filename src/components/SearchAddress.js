import React from 'react';
import styled from 'styled-components';

import SearchIcon from '../assets/search.svg';

const SearchbarContainer = styled.div`
    font-family: 'Nunito', sans-serif;
    position: absolute;
    left: 0; 
    right: 0; 
    margin: auto;
    display: flex;
    padding: 8px 14px;
    width: 345px;
    height: 35px;
    align-items: center;
    font-weight: 700;
    background: #F3F3F3;
    color: rgba(220, 76, 82, 0.6);
    border-radius: 30px;
    top: 5vh;
    z-index: 10;
    box-shadow: -2px 10px 20px -3px rgba(0, 0, 0, 0.25);

    input {
        all: unset;
        text-align: left;
        width: 90%;
        text-color: rgba(220, 76, 82, 0.6);
    }

    input::placeholder { /* Chrome, Firefox, Opera, Safari 10.1+ */
        color: rgba(220, 76, 82, 0.6);
      }
      
    input::-ms-input-placeholder { /* Internet Explorer 10-11 */
        color: rgba(220, 76, 82, 0.6);
      }
      
    input::-ms-input-placeholder { /* Microsoft Edge */
        color: rgba(220, 76, 82, 0.6);
      }
`;

const SearchAddress = () => {
    return (
        <SearchbarContainer>
            <input placeholder="Address or name..." name="search" />
            <img src={SearchIcon} alt="Search" />
        </SearchbarContainer>
    );
};

export default SearchAddress;