import React from 'react';
import styled from 'styled-components';

const CustomButton = styled.button`
    all: unset;
    background: ${props => props.type === 'primary' ? '#E2AF9E' : '#FFFFFF'};
    padding: 4px 8px;
    width: 220px;
    height: 46px;
    border: 2px solid #DC4C52;
    border-radius: 14px;
    color: #FFFFFF;
    z-index: 100;
    
    box-shadow: 0 4px 4px 0 rgba(0, 0, 0, 0.25);
    text-shadow: 0 0 1px black, 0 0 1px black, 0 0 1px black, 0 0 1px black, 0 0 1px black, 0 0 1px black, 0 0 1px black, 0 0 1px black, 0 0 1px black, 0 0 1px black, 0 0 1px black, 0 0 1px black, 0 0 1px black, 0 0 1px black, 0 0 1px black, 0 0 1px black, 0 0 1px black, 0 0 1px black, 0 0 1px black, 0 0 1px black;
    
    &:hover {
        cursor: pointer;
        background: ${props => props.type === 'primary' ? '#E2AF8F' : '#EEEEEE'};
    }

    span {
        font-family: 'Nunito', sans-serif;
        font-size: 22px;
        font-weight: 700;
    }
`;

const Button = ({ type, children }) => {
    return (
        <CustomButton type={type}><span>{children}</span></CustomButton>
    );
};

export default Button;