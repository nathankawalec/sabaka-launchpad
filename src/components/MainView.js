import React from 'react';
import styled from 'styled-components';

import SearchAddress from './SearchAddress';
import Button from './Button';

const Container = styled.div`
    display: flex;
    flex-direction: column;
    z-index: 10;
`;

const NavbarButtons = styled.div`
    position: absolute;
    top: 4vh;
    right: 3vw;
    display: flex;
    width: 100%;
    align-items: center;
    justify-content: flex-end;
`;

const PresaleContainer = styled.div`
    display: flex;
    flex-direction: column;
    width: 100%;
    height: 80vh;
    justify-content: flex-end;
    align-items: center;
`;

const MainView = () => {
    return (
        <Container>
            <SearchAddress />
            <NavbarButtons>
                <Button type="primary">
                    Connect Wallet
                </Button>
            </NavbarButtons>
            <PresaleContainer>
            <Button type="secondary">
                    Purchase
            </Button>
            </PresaleContainer>
        </Container>
    );
};

export default MainView;