import React from 'react';
import styled from 'styled-components';
import MainView from './components/MainView';

import Logo from './assets/logo.svg';
import Stripe from './assets/stripe.svg';

const AppContainer = styled.div`
	text-align: center;
	position: relative;
	background: #FBF3EC;
	overflow-x: hidden;
	overflow-y: hidden;
`;

const OvalBackground = styled.div`
    position: absolute;
    width: 80vw;
    height: 100vh;
	top:  ${props => props.type === 'primary' ? '55%' : '70%'};
	left: ${props => props.type === 'primary' ? '40%' : '-10%'};
    background: ${props => props.type === 'primary' ? 'rgba(229, 181, 128, 0.3)' : 'rgba(239, 216, 190, 0.3)'};
    border-radius: 50%;
	z-index: ${props => props.type === 'primary' ? '2' : '1'};
`;

const LogoContainer = styled.img`
	position: absolute;
	top: 60%;
	left: 82%;
	z-index: 2;
	height: 38vh;
`;


const StripeContainer = styled.img`
	position: absolute;
	top: -2%;
	left: 1%;
	z-index: 2;
	height: 105vh;
`;

const App = () => {
	return (
		<AppContainer>
			<MainView />
			<OvalBackground type='primary' />
			<OvalBackground type='secondary' />
			<LogoContainer src={Logo} alt="Logo" />
			<StripeContainer src={Stripe} alt="Stripe" />
		</AppContainer>
	)
}

export default App;
